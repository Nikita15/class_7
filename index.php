<?php

	class Product
	{
		public $name;
		public $nameFontSize;
		public $price;
		public $priceFontSize;
		public $weight;
		public $weightFontSize;
		public $border;
		public $bg;

		public function __construct(string $name, int $price, float $weight, string $border, string $bg, int $nameFontSize = 20, int $priceFontSize = 16, int $weightFontSize = 16)
		{
			$this->name = $name;
			$this->nameFontSize = $nameFontSize;
			$this->price = $price;
			$this->priceFontSize = $priceFontSize;
			$this->weight = $weight;
			$this->weightFontSize = $weightFontSize;
			$this->border = $border;
			$this->bg = $bg;
		}

		public function printProduct()
		{
			echo "<div style='border: {$this->border}; background: {$this->bg};'>
			<h2 style='font-size: {$this->nameFontSize}px'> {$this->name} </h2>
			<span>	Цена: {$this->price} $ <br> Вес: {$this->weight} Kg </span>
			</div>";
		}
	}

	class PrintPrice extends Product
	{
		public $priceNotNds;
		public $priceNotNdsFontSize;

		public function __construct(string $name, int $price, int $priceNotNds, float $weight, string $border, string $bg, int $nameFontSize = 20, int $priceFontSize = 16, int $priceNotNdsFontSize = 16, int $weightFontSize = 16)
		{
			$this->priceNotNds = $priceNotNds;
			$this->priceNotNdsFontSize = $priceNotNdsFontSize;
		
			parent::__construct($name, $price, $weight, $border, $bg, $nameFontSize, $priceFontSize, $weightFontSize);
		}

		public function printProduct()
		{
			echo "<div>
			<h2 style='font-size: {$this->nameFontSize}px'>{$this->name} </h2>
			<span> Стоимость товара: {$this->price} $ <br> Стоимость товара без учёта НДС: {$this->priceNotNds} $ </span>
			</div>";
		}

	}

	echo "<div style= background: aqua;'><h2><center> Домашнее задание №7 </center></h2></div>";
	echo '<br>';
	$productsNews = new Product('Pineapple', '150', '1.0', '1px solid black', 'aqua');
	$productsNews -> printProduct();
	$printPriceNews = new PrintPrice('Order 1', '150', '140', '1.0', '1px solid black', 'aqua');
	$printPriceNews -> printProduct();
	echo '<br>';
	echo '<br>';
	$productsNews1 = new Product('Banana', '20', '0.2', '1px solid black', 'aqua');
	$productsNews1 -> printProduct();
	$printPriceNews1 = new PrintPrice('Order 2', '20', '18', '0.2', '1px solid black', 'aqua');
	$printPriceNews1 -> printProduct();
	echo '<br>';
	echo '<br>';
	$productsNews2 = new Product('Order 3', '20', '0.1', '1px solid black', 'aqua');
	$productsNews2 -> printProduct();
	$printPriceNews2 = new PrintPrice('Order 3', '20', '18', '0.1', '1px solid black', 'aqua');
	$printPriceNews2 -> printProduct();

?>